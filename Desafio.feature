História: Chamada de video para um contato
Como usuário do Whatsapp, quero efetuar chamadas de video para meus contatos, reduzindo gastos

Cenário: Efetuar uma videochamada 
	Dado que eu abri a tela de bate-papo do contato 
	Quando toco o ícone da camera no topo da tela
	E toco a opção chamar
	Então a videochamada deve ser feita
	
Cenário: Cancelar uma videochamada 
	Dado que eu abri a tela de bate-papo do contato 
	Quando toco o ícone da camera no topo da tela 
	E toco a opção cancelar 
	Então a videochamada deve ser cancelada
	
História: Efetuar manutenção de contatos e grupos
Como usuário do Whatsapp, quero poder criar e remover grupos e contatos

Cenário: Pesquisar um contato
	Dado que eu abri a tela de conversas do aplicativo
	Quando toco o ícone lupa no topo da tela
	E digito o nome do contato
	Então é exibida a lista dos contatos que contém o nome pesquisado
	
Cenário: Remover uma conversa
	Dado que eu abri a tela de conversas do aplicativo
	Quando toco sobre a conversa por alguns instantes
	E toco no ícone da lixeira no topo da tela
	E toco em apagar
	Então então a conversa é excluída da tela de conversas
	
Cenário: Criar um novo grupo
	Dado que eu abri a tela de conversas do aplicativo
	Quando toco nos três pontos no topo da tela
	E toco na opção novo grupo
	E seleciono os contatos
	E no ícone para prosseguir
	E informo o nome do grupo
	E aciono o ícone de confirmação
	Então então o novo grupo é criado
	
Cenário: Adicionar novo contato ao grupo
	Dado que eu abri a tela de bate-papo do grupo
	Quando toco nos três pontos no topo da tela
	E toco na opção dados do grupo
	E toco na opção adicionar participante
	E toco em um contato
	E toco em ok
	Então então o contato é adicionado ao grupo
	
Cenário: Remover um contato do grupo
	Dado que eu abri a tela de dados do grupo
	Quando toco nos três pontos no topo da tela
	E toco em um participante
	E toco na opção remover o participante
	E toco em ok
	Então então o participante é removido do grupo

Cenário: Remover um grupo
	Dado que eu abri a tela de dados do grupo
	Quando toco em apagar grupo
	E toco em apagar
	Então então o grupo é removido
	
Cenário: Alterar o tom de notificação do grupo
	Dado que eu abri uma tela de bate-papo do grupo 
	Quando toco o ícone de 3 pontos no topo da tela 
	E toco em dados do grupo 
	E toco notificações personalizadas 
	E toco na caixa de marca personalizar 
	E toco no som de notificação 
	E toco no som desejado
	E toco o ícone de 3 pontos no topo da tela 
	E toco em redefinir configurações de notificação
	Então o som de notificação é configurado

